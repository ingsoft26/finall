﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FrmMain
{
    public partial class FrmConsultas : Form
    {
        public FrmConsultas()
        {
            InitializeComponent();
        }

        public void mostrarBotonYBox() {

            boxCiudades.Visible = true;
            btnBuscar.Visible = true;


        }
        public void OcultarBotonYBox()
        {

            boxCiudades.Visible = false;
            btnBuscar.Visible = false;


        }

        public Conexion con = new Conexion();

        public void vistaTabla1(string instruccion) {

           
            con.AbrirConexion();

            
            MySqlDataAdapter adp = new MySqlDataAdapter(instruccion,con.varConexion);
            DataTable Consulta = new DataTable();
            adp.Fill(Consulta);

            dataGridView1.DataSource = Consulta;

            con.CerrarConexion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            vistaTabla2();


        }

        public void vistaTabla2()
        {
            string ciudad = boxCiudades.Text;

            con.AbrirConexion();


            MySqlDataAdapter adp = new MySqlDataAdapter(String.Format("SELECT * FROM cines WHERE Ciudad=('{0}')", ciudad), con.varConexion);
            DataTable Consulta = new DataTable();
            adp.Fill(Consulta);

            dataGridView1.DataSource = Consulta;
            con.CerrarConexion();


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;


namespace FrmMain
{
    public class Usuario
    {
        //Parámetros
        private string usuario;
        private string contraseña;
        public Conexion con = new Conexion();

        //getters and setters
        public void setUser(string us) {

            this.usuario = us;

        }

        public string getUser() {

            return usuario;
        }

        public void setPass(string pas) {

            this.contraseña = pas;

        }

        public string getPas() {


            return contraseña;

        }

        //Constructores
        public Usuario(string user, string pass) {

            this.usuario = user;
            this.contraseña = pass;
            
        }

        public Usuario() {}

        //                         MÉTODOS
        //-------------------------------------------------------

        public Boolean Loggearse(string us, string pas) {

           
            string x="";
            string y = "";
            try
            {   
                if(con.AbrirConexion()==true)
                {
                    x = buscarUsuario(con.varConexion, us);
                    y = buscarContraseña(con.varConexion, pas);
                    con.CerrarConexion();
                }
                 

            }
            catch (MySql.Data.MySqlClient.MySqlException ex) {

                
                MessageBox.Show("Usuario no encontrado o contraseña incorrecta, por favor registrese o vuelva a intentarlo ");
                return false;
            }
            if ((x==us)&&(y==pas)) 

                return true;

             else

                 return false;

            
        }
        public void Registrarse() {

            

            

           

            try {

                if (con.AbrirConexion() == true)
                {

                    MySqlCommand comando = new MySqlCommand(String.Format("INSERT INTO usuarios(Usuario, Contraseña) VALUES('{0}', '{1}')", usuario, contraseña), con.varConexion);
                    comando.ExecuteNonQuery();


                    con.CerrarConexion();
                }

            } catch(MySql.Data.MySqlClient.MySqlException ex)  {

                MessageBox.Show(ex.Message);

            }



        }

        public string buscarUsuario(MySqlConnection conexion,string user) {

            string x = "";
            
            MySqlCommand comando = new MySqlCommand(String.Format("SELECT Usuario FROM usuarios WHERE Usuario=('{0}')",user),conexion);
            MySqlDataReader reader= comando.ExecuteReader(); 


            reader.Read();
            

             x = reader.GetString(0);


            
            reader.Close();

            return x;
        }

        public string buscarContraseña(MySqlConnection conexion, string pass) {


            String y = "";


            MySqlCommand comando = new MySqlCommand(String.Format("SELECT Contraseña FROM Usuarios WHERE Contraseña=('{0}')", pass), conexion);
            MySqlDataReader reader = comando.ExecuteReader();

            reader.Read();


           y = reader.GetString(0);
            reader.Close();
            return y;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace FrmMain
{
    public class Pelicula
    {
        public string peliculaEncontrada = "";
        public string horario = "";
        public string precio = "";

        public void buscar(string nombrepeli)
        {
            FrmPelicula formpel = new FrmPelicula();

            
            
            
            Conexion con = new Conexion();

            con.AbrirConexion();
            try
            {
                MySqlCommand comando = new MySqlCommand(String.Format("SELECT NombrePelicula FROM Películas WHERE NombrePelicula=('{0}')", nombrepeli), con.varConexion);
                MySqlDataReader reader = comando.ExecuteReader();
                reader.Read();
                

                peliculaEncontrada = reader.GetString(0);
                reader.Close();

                MySqlCommand comando2 = new MySqlCommand(String.Format("SELECT Horario FROM Películas WHERE NombrePelicula=('{0}')", nombrepeli), con.varConexion);
                reader = comando2.ExecuteReader();
                reader.Read();

                horario = reader.GetString(0);
                reader.Close();

                MySqlCommand comando3 = new MySqlCommand(String.Format("SELECT Precio FROM Películas WHERE NombrePelicula=('{0}')", nombrepeli), con.varConexion);
                reader = comando3.ExecuteReader();
                reader.Read();

                precio = reader.GetString(0);
                reader.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex) {

                MessageBox.Show("Película no encontrada en la Base de Datos");
            }


        }




    }
}

﻿namespace FrmMain
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnCinesRegistrados = new System.Windows.Forms.Button();
            this.btnCinesXCiudad = new System.Windows.Forms.Button();
            this.btnBuscarPeli = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(226, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome, please click in a button";
            // 
            // btnCinesRegistrados
            // 
            this.btnCinesRegistrados.BackColor = System.Drawing.Color.White;
            this.btnCinesRegistrados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCinesRegistrados.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCinesRegistrados.Location = new System.Drawing.Point(117, 197);
            this.btnCinesRegistrados.Name = "btnCinesRegistrados";
            this.btnCinesRegistrados.Size = new System.Drawing.Size(165, 60);
            this.btnCinesRegistrados.TabIndex = 1;
            this.btnCinesRegistrados.Text = "Consultar Cines Registrados";
            this.btnCinesRegistrados.UseVisualStyleBackColor = false;
            this.btnCinesRegistrados.Click += new System.EventHandler(this.btnCinesRegistrados_Click);
            // 
            // btnCinesXCiudad
            // 
            this.btnCinesXCiudad.BackColor = System.Drawing.Color.White;
            this.btnCinesXCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCinesXCiudad.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCinesXCiudad.Location = new System.Drawing.Point(483, 197);
            this.btnCinesXCiudad.Name = "btnCinesXCiudad";
            this.btnCinesXCiudad.Size = new System.Drawing.Size(165, 60);
            this.btnCinesXCiudad.TabIndex = 3;
            this.btnCinesXCiudad.Text = "Consultar Cines por Ciudad";
            this.btnCinesXCiudad.UseVisualStyleBackColor = false;
            this.btnCinesXCiudad.Click += new System.EventHandler(this.btnCinesXCiudad_Click);
            // 
            // btnBuscarPeli
            // 
            this.btnBuscarPeli.BackColor = System.Drawing.Color.White;
            this.btnBuscarPeli.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarPeli.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarPeli.Location = new System.Drawing.Point(301, 197);
            this.btnBuscarPeli.Name = "btnBuscarPeli";
            this.btnBuscarPeli.Size = new System.Drawing.Size(165, 60);
            this.btnBuscarPeli.TabIndex = 5;
            this.btnBuscarPeli.Text = "Buscar Películas";
            this.btnBuscarPeli.UseVisualStyleBackColor = false;
            this.btnBuscarPeli.Click += new System.EventHandler(this.btnBuscarPeli_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::FrmMain.Properties.Resources.salir;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(671, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 63);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::FrmMain.Properties.Resources.FondoMain;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(746, 319);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBuscarPeli);
            this.Controls.Add(this.btnCinesXCiudad);
            this.Controls.Add(this.btnCinesRegistrados);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "                                                                                 " +
    "             BASE DE DATOS DE CINE";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCinesRegistrados;
        private System.Windows.Forms.Button btnCinesXCiudad;
        private System.Windows.Forms.Button btnBuscarPeli;
        private System.Windows.Forms.Button button1;
    }
}


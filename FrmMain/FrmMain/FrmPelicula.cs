﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmMain
{
    public partial class FrmPelicula : Form
    {
        public FrmPelicula()
        {
            InitializeComponent();
        }

        private void FrmPelicula_Load(object sender, EventArgs e)
        {

        }

        

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Pelicula peli = new Pelicula();

            peli.buscar(boxNombrePelicula.Text);

            lblName.Text = peli.peliculaEncontrada;
            lblHorario.Text = peli.horario;
            lblBoleta.Text = peli.precio;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmMain
{
    public partial class Loggeo : Form
    {
        public Loggeo()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Usuario user = new Usuario();

           Boolean x =  user.Loggearse(boxUser.Text,boxPass.Text);

            string use = boxUser.Text;
            if (x == true)
            {

                MessageBox.Show("Bienvenido " + use);
                this.Close();
            }
            
        }

        private void Loggeo_Load(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            Usuario un = new Usuario(boxUser.Text,boxPass.Text);
            un.Registrarse();

        }
    }
}

